"""Bootable Image build element

A :mod:`ScriptElement <buildstream.scriptelement>` implementation for creating
Bootable Images

The bootableImage default configuration:
  .. literalinclude:: bootableImage.yaml
     :language: yaml
"""
import os

from buildstream import ScriptElement, ElementError, Scope


# Element implementation for the '' kind.
class bootableImage(ScriptElement):
    def get_unique_key(self):
        uqk = super().get_unique_key()
        uqk['images'] = self.partition_data
        uqk['export_partitions'] = self.export_partitions
        uqk['invalidate'] = 16
        return uqk
    def stage(self, sandbox):
        super().stage(sandbox)
        conf_file = os.path.join(sandbox.get_directory(), 'images', 'genimage-run.cfg')

        with open(conf_file, 'w') as fl:
            for imageDic in self.partition_data:
                if imageDic["fstype"] == "fat":
                    fl.write("""image boot.vfat {
  vfat {
                    """)
                    for filename in os.listdir(os.path.join(sandbox.get_directory(), 'images/raw', imageDic["fsname"])):

                        fl.write("    file " + filename + " { image = \"../raw/"+imageDic["fsname"]+"/"+filename +"\" }\n")
                    fl.write("""  }}
  size = {}
}}
""".format(imageDic['fssize']))

            fl.write("""
image sdcard.img {
  hdimage {
  }
""")
            for imageDic in self.partition_data:

                if imageDic["fstype"] == "fat":
                     fl.write("""
  partition boot {
    partition-type = 0xC
    bootable = "true"
    image = "boot.vfat"
  }""")
                if imageDic["fstype"] == "ext4":
                    fl.write("""

  partition rootfs {
    partition-type = 0x83
    image = "rootfs.ext4"
  }
""")#.format(imageDic["fsname"]))
            fl.write("""
}""")


    def configure(self, node):
        command_steps = [
            "create_partitions",
            "create_image",
        ]

        self.node_validate(node,
            command_steps + ["partitions", "gen_image", "export_partitions", "image_tools"])

        self.export_partitions = self.node_get_member(node, str, "export_partitions")

        for subset in self.node_get_member(node, list, 'image_tools'):
            self.layout_add(subset, "/")

#        allDeps = self.dependencies(Scope.BUILD)
#        for dep in allDeps:
#            print ('dep', dep)
#        print('allDeps', list(allDeps))

        self.layout_add(None, '/images/')
        self.layout_add(None, '/images/input')
        self.layout_add(None, '/images/raw')
        self.layout_add(None, '/images/root')

#        self.layout_add(None, '/images/root')

        self.partition_data = []
        self.partition_commands = []
        print(self.node_get_member(node, list, 'partitions'))
        for subset in self.node_get_member(node, list, 'partitions'):
            print('bit', subset)
            fsname = self.node_get_member(subset, str, 'name')
            fselement = self.node_get_member(subset, str, 'element')
            fstype = self.node_get_member(subset, str, 'filesystem')
            fssize = self.node_get_member(subset, str, 'size')
            self.layout_add(fselement,
                            "/images/raw/" + str(fsname))
            self.partition_data.append({"fsname": fsname,
                                    "fselement": fselement,
                                    "fstype": fstype,
                                    "fssize": fssize,
            })
            if fstype == "ext4":
                self.partition_commands.append("""cd images/input
truncate -s {}  rootfs.ext4
mkfs.ext4 -F -i 8192 -L root -d ../raw/userland rootfs.ext4""".format(fssize))



        for step in command_steps:
            if step not in node:
                raise ElementError("{}: Unexpectedly missing command step '{}'"
                                   .format(self, step))
            cmds = self.node_subst_list(node, step)
            if step == "create_partitions":
                cmds += self.partition_commands
            self.add_commands(step, cmds)
        if self.export_partitions == 'true':
            self.add_commands('export partition', ['mv /images/input/* /buildstream-install/',])
        self.set_work_dir()
        self.set_install_root()
        self.set_root_read_only(True)


# Plugin entry point
def setup():
    return bootableImage

